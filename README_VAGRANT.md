Sobre o Vagrant
===============

O Vagrant é um projeto que permite virtualizar o ambiente de desenvolvimento de forma simples. 
Com o Vagrant você pode executar máquinas virtuais utilizando o VirtualBox ou VMware, dentre 
muitas outras opções. Estas máquinas virtuais podem ter qualquer configuração e programas 
instalados e você pode, inclusive, criar a sua própria configuração com muita facilidade. 
Isso ajuda bastante quando um novo funcionário é contratado, por exemplo. Se todo o seu 
ambiente de desenvolvimento for baseado em boxes (box é o nome que o Vagrant utilizada 
para definir cada máquina virtual) personalizados, tudo o que ele precisa fazer é instalar 
o Vagrant e executar um único comando.

Instalando o Vagrant
--------------------

1 - Instale o [VirtualBox](https://www.virtualbox.org/wiki/Downloads)

2 - Instale o [Vagrant](https://www.vagrantup.com/downloads.html)

3 - Crie uma Imagem com todos os requisitos para rodas python ([Clique para mais Informações](http://cotidianolinux.com.br/off-python/))

4 - Baixe e instale 2 programas (putty.exe e puttygen.exe)

5 - Execute o programa puttygen.exe, click em Generate (gerando uma chave SSH)

6 - Click em LOAD e va na pasta documentos/vagrant.d/insecure_private_key

7 - Salve a chave privada na mesma pasta com o nome: vagrant.ppk

Utilize os comando abaixo para usar e inciar o Vagrant.

Comando do Vagrant
------------------

Para inciar uma VM Vagrant

    # vagrant init NOME_DA_VM_VAGRANT

Instalar um plugins para atualização automatica da VirtualBox

    # vagrant plugin install vagrant-vbguest

Recupera uma Virtual Machine para um novo Box Vagrant

    # vagrant package --output NAME.box

Incluir uma Box dentro do gerenciador do Vagrant

    # vagrant box add NAME_BOX  /PATH.box

Inciar uma nova vagrant box

    # vagrant init NAME_BOX

Ativar/Levantar/Iniciar uma box

    # vagrant up

Deletar uma VagrantBox

    # vagrant box remove VAGRANT_BOX

Atualizar uma Box existente no Vagrant box

    # vagrant box update

Mostrar listagem de box que estão no vagrant

    vagrant box list

Mais detalhes: [Video de Instalação do Vagrant](https://vimeo.com/82343803)

