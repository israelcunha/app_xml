# xml_viewer 

## Configuração do Ambiente de desenvolvimento

Comandos Básicos do Django no Windows 
-------------------------------------

Antes de começar o procedimento é necessário acessar o pronpt de comando do windows.

Criar a Virtualenv

    python -m venv myvenv

Ativar a Virtualenv

    myvenv\Scripts\activate

Exemplo da máquina virtual funcionando:

    (myvenv) C:\Usuário\Nome\<myProject>

Realizand a instalação do Django

    # (myvenv) ~$ pip install django==1.8.5 
    ## ou (myvenv) ~$ pip install django -> este pega a versão mais recente
    Downloading/unpacking django==1.8.5
    Installing collected packages: django
    Successfully installed django
    Cleaning up...

Após Instalar o Django, seu ambiente esta pronto.
O comando abaixo vai criar seu projeto Django, colocando sua biblioteca e montando sua estrutura.

    python myvenv\Scripts\django-admin.py startproject <mysite>

Para inciar o servidor basta este comando, o servidor vai estar na link: http://127.0.0.1:8000/

    python manage.py runserver


Para manter tudo arrumado vamos criar um aplicativo separado dentro do nosso projeto. 
É muito bom ter tudo organizado desde o início. Para criar um aplicativo precisamos 
executar o seguinte comando no console:

    python manage.py startapp core

Para criar os modelos no banco de dados utilize o seguinte comando.

    python manage.py migrate


Instalar Vagrant
----------------
	
	
[Vagrant Install - CLICK AQUI](README_VAGRANT.md)



Sites Uteis
-----------

[Django Documentation](https://docs.djangoproject.com/en/1.10/)

[Django Packages](https://djangopackages.org/)

[Code Font Django](https://github.com/django/django)
